using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public GameObject bullet;
    GameObject tempBullet; 
    public Transform firePoint;
    private float bulletSpeed = 23f;
    Vector3 lookPos;
    bool flag;
    float tiempo_start;
    float tiempo_end = 4;
    float coldownStart = 4;
    float coldownEnd = 4;


    private void Start()
    {
        flag = false;
        tiempo_end = 0.4f;
        coldownStart = 0.4f;
        coldownEnd = 0.4f;
    }

    void Update()
    {
        Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
        lookPos = Camera.main.ScreenToWorldPoint(mousePos) - transform.position;
        if (!flag)
        {
            tiempo_start = 0;
            if (coldownStart >= coldownEnd)
            {
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    PlayerShooting();
                    flag = true;
                }
            }
            else
            {
                coldownStart += Time.deltaTime;
            }
        }
        else
        {
            if (tiempo_start >= tiempo_end)
            {
                coldownStart = 0;
                flag = false;
            }
            else
            {
                tiempo_start += Time.deltaTime;
            }

        }


    }
    private void PlayerShooting()
    {
        tempBullet = Instantiate(bullet, firePoint.position, firePoint.rotation);
        Rigidbody2D rb = tempBullet.GetComponent<Rigidbody2D>();
        rb.AddForce(firePoint.right * bulletSpeed, ForceMode2D.Impulse);
    }

    

}