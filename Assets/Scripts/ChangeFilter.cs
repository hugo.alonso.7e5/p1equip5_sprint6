using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeFilter : MonoBehaviour //:GameManager
{
    public Wilberforce.Colorblind Colorblind;
    public static int TypeFilter;
    public int distancia;
    public UIManager gm;
    private float time;
    private float timeDelay;
    // Start is called before the first frame update
    void Start()
    {
        //distancia = UIManager.distance;
        //Pillar el GameManager
        //gm = GetComponent<UIManager>(); 
        Colorblind = Camera.main.GetComponent<Wilberforce.Colorblind>();
        time = 0f;
        timeDelay = 4f;
        //Coger la distacnia que lleva el jugador
    }

    // Update is called once per frame
    void Update()
    {
        distancia = UIManager.distance;
        Debug.Log(distancia);
        time += 1f * Time.deltaTime;
        //Poner ifs segun distancia con los filtros

        if (distancia <= 50)
        {
            TypeFilter = 0;
        }
        else if (distancia <= 100)
        {
            TypeFilter = 1;
        }
        else if (distancia <= 150)
        {
            TypeFilter = 2;
        }
        else if (distancia <= 200)
        {
            TypeFilter = 3;
        }
        else if (distancia >= 200)
        {
            TypeFilter = 0;
        }
        Colorblind.Type = TypeFilter;
    }
}
