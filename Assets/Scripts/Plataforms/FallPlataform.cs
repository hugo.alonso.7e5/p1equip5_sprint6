using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallPlataform : MonoBehaviour
{
    int type = ChangeFilter.TypeFilter;

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (type != 0 && collision.gameObject.tag == ("Player"))
        {
            transform.position -= new Vector3(0f, 0.15f, 0f);
        }
    }
}
