using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformSpawner : MonoBehaviour
{
    public GameObject plataforma1, plataforma2, plataforma3, plataforma4, plataforma5;
    public GameObject protPlataforma1, protPlataforma2, protPlataforma3, protPlataforma4, protPlataforma5;
    public GameObject deutanPlataforma1, deutanPlataforma2, deutanPlataforma3, deutanPlataforma4, deutanPlataforma5;
    public GameObject tritanPlataforma1, tritanPlataforma2, tritanPlataforma3, tritanPlataforma4, tritanPlataforma5;
    bool hasground = true;
    public static int plataformNum;
    int anterior;

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if (!hasground)
        {
            SpawnPlataform();
            hasground = true;
        }
    }

    public void SpawnPlataform()
    {




        Debug.Log(plataformNum);

        if (ChangeFilter.TypeFilter != 1)
        {
            while (plataformNum == anterior)
            {
                plataformNum = Random.Range(1, 21);
            }

            if (plataformNum == 1) Instantiate(plataforma1, new Vector3(transform.position.x + 12, -3.0f, 0), Quaternion.identity);
            if (plataformNum == 2) Instantiate(plataforma2, new Vector3(transform.position.x + 8, -2.0f, 0), Quaternion.identity);
            if (plataformNum == 3) Instantiate(plataforma3, new Vector3(transform.position.x + 9, 0f, 0), Quaternion.identity);
            if (plataformNum == 4) Instantiate(plataforma4, new Vector3(transform.position.x + 6, -4.0f, 0), Quaternion.identity);
            if (plataformNum == 5) Instantiate(plataforma5, new Vector3(transform.position.x + 4, -2.5f, 0), Quaternion.identity);
            if (plataformNum == 6) Instantiate(protPlataforma1, new Vector3(transform.position.x + 12, -3.0f, 0), Quaternion.identity);
            if (plataformNum == 7) Instantiate(protPlataforma2, new Vector3(transform.position.x + 8, -2.0f, 0), Quaternion.identity);
            if (plataformNum == 8) Instantiate(protPlataforma3, new Vector3(transform.position.x + 9, 0f, 0), Quaternion.identity);
            if (plataformNum == 9) Instantiate(protPlataforma4, new Vector3(transform.position.x + 6, -4.0f, 0), Quaternion.identity);
            if (plataformNum == 10) Instantiate(protPlataforma5, new Vector3(transform.position.x + 4, -2.5f, 0), Quaternion.identity);
            if (plataformNum == 11) Instantiate(deutanPlataforma1, new Vector3(transform.position.x + 12, -3.0f, 0), Quaternion.identity);
            if (plataformNum == 12) Instantiate(deutanPlataforma2, new Vector3(transform.position.x + 8, -2.0f, 0), Quaternion.identity);
            if (plataformNum == 13) Instantiate(deutanPlataforma3, new Vector3(transform.position.x + 9, 0f, 0), Quaternion.identity);
            if (plataformNum == 14) Instantiate(deutanPlataforma4, new Vector3(transform.position.x + 6, -4.0f, 0), Quaternion.identity);
            if (plataformNum == 15) Instantiate(deutanPlataforma5, new Vector3(transform.position.x + 4, -2.5f, 0), Quaternion.identity);
            if (plataformNum == 16) Instantiate(tritanPlataforma1, new Vector3(transform.position.x + 12, -3.0f, 0), Quaternion.identity);
            if (plataformNum == 17) Instantiate(tritanPlataforma2, new Vector3(transform.position.x + 8, -2.0f, 0), Quaternion.identity);
            if (plataformNum == 18) Instantiate(tritanPlataforma3, new Vector3(transform.position.x + 9, 0f, 0), Quaternion.identity);
            if (plataformNum == 19) Instantiate(tritanPlataforma4, new Vector3(transform.position.x + 6, -4.0f, 0), Quaternion.identity);
            if (plataformNum == 20) Instantiate(tritanPlataforma5, new Vector3(transform.position.x + 4, -2.5f, 0), Quaternion.identity);
            anterior = plataformNum;
        }
        else if (ChangeFilter.TypeFilter == 1)
        {
            while (plataformNum == anterior)
            {
                plataformNum = Random.Range(1, 11);
            }
            if (plataformNum == 1) Instantiate(plataforma1, new Vector3(transform.position.x + 12, -3.0f, 0), Quaternion.identity);
            if (plataformNum == 2) Instantiate(plataforma2, new Vector3(transform.position.x + 8, -2.0f, 0), Quaternion.identity);
            if (plataformNum == 3) Instantiate(plataforma3, new Vector3(transform.position.x + 9, 0f, 0), Quaternion.identity);
            if (plataformNum == 4) Instantiate(plataforma4, new Vector3(transform.position.x + 6, -4.0f, 0), Quaternion.identity);
            if (plataformNum == 5) Instantiate(plataforma5, new Vector3(transform.position.x + 4, -2.5f, 0), Quaternion.identity);
            if (plataformNum == 6) Instantiate(protPlataforma1, new Vector3(transform.position.x + 12, -3.0f, 0), Quaternion.identity);
            if (plataformNum == 7) Instantiate(protPlataforma2, new Vector3(transform.position.x + 8, -2.0f, 0), Quaternion.identity);
            if (plataformNum == 8) Instantiate(protPlataforma3, new Vector3(transform.position.x + 9, 0f, 0), Quaternion.identity);
            if (plataformNum == 9) Instantiate(protPlataforma4, new Vector3(transform.position.x + 6, -4.0f, 0), Quaternion.identity);
            if (plataformNum == 10) Instantiate(protPlataforma5, new Vector3(transform.position.x + 4, -2.5f, 0), Quaternion.identity);

            anterior = plataformNum;
        }
        else if (ChangeFilter.TypeFilter == 2)
        {
            while (plataformNum == anterior)
            {
                plataformNum = Random.Range(1, 11);
            }
            if (plataformNum == 1) Instantiate(plataforma1, new Vector3(transform.position.x + 12, -3.0f, 0), Quaternion.identity);
            if (plataformNum == 2) Instantiate(plataforma2, new Vector3(transform.position.x + 8, -2.0f, 0), Quaternion.identity);
            if (plataformNum == 3) Instantiate(plataforma3, new Vector3(transform.position.x + 9, 0f, 0), Quaternion.identity);
            if (plataformNum == 4) Instantiate(plataforma4, new Vector3(transform.position.x + 6, -4.0f, 0), Quaternion.identity);
            if (plataformNum == 5) Instantiate(plataforma5, new Vector3(transform.position.x + 4, -2.5f, 0), Quaternion.identity);
            if (plataformNum == 6) Instantiate(deutanPlataforma1, new Vector3(transform.position.x + 12, -3.0f, 0), Quaternion.identity);
            if (plataformNum == 7) Instantiate(deutanPlataforma2, new Vector3(transform.position.x + 8, -2.0f, 0), Quaternion.identity);
            if (plataformNum == 8) Instantiate(deutanPlataforma3, new Vector3(transform.position.x + 9, 0f, 0), Quaternion.identity);
            if (plataformNum == 9) Instantiate(deutanPlataforma4, new Vector3(transform.position.x + 6, -4.0f, 0), Quaternion.identity);
            if (plataformNum == 10) Instantiate(deutanPlataforma5, new Vector3(transform.position.x + 4, -2.5f, 0), Quaternion.identity);
            anterior = plataformNum;
        }
        else if (ChangeFilter.TypeFilter == 3)
        {
            while (plataformNum == anterior)
            {
                plataformNum = Random.Range(1, 11);
            }

            if (plataformNum == 1) Instantiate(plataforma1, new Vector3(transform.position.x + 12, -3.0f, 0), Quaternion.identity);
            if (plataformNum == 2) Instantiate(plataforma2, new Vector3(transform.position.x + 8, -2.0f, 0), Quaternion.identity);
            if (plataformNum == 3) Instantiate(plataforma3, new Vector3(transform.position.x + 9, 0f, 0), Quaternion.identity);
            if (plataformNum == 4) Instantiate(plataforma4, new Vector3(transform.position.x + 6, -4.0f, 0), Quaternion.identity);
            if (plataformNum == 5) Instantiate(plataforma5, new Vector3(transform.position.x + 4, -2.5f, 0), Quaternion.identity);
            if (plataformNum == 6) Instantiate(tritanPlataforma1, new Vector3(transform.position.x + 12, -3.0f, 0), Quaternion.identity);
            if (plataformNum == 7) Instantiate(tritanPlataforma2, new Vector3(transform.position.x + 8, -2.0f, 0), Quaternion.identity);
            if (plataformNum == 8) Instantiate(tritanPlataforma3, new Vector3(transform.position.x + 9, 0f, 0), Quaternion.identity);
            if (plataformNum == 9) Instantiate(tritanPlataforma4, new Vector3(transform.position.x + 6, -4.0f, 0), Quaternion.identity);
            if (plataformNum == 10) Instantiate(tritanPlataforma5, new Vector3(transform.position.x + 4, -2.5f, 0), Quaternion.identity);
            anterior = plataformNum;
        }
        anterior = plataformNum;
    }
    public void SpawnPlataformProt()
    {
        while (plataformNum == anterior)
        {
            plataformNum = Random.Range(1, 6);
        }

        Debug.Log(plataformNum);


        anterior = plataformNum;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Ground")) hasground = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Ground")) hasground = false;
    }

}


