using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HunterShoot : MonoBehaviour
{
    public AudioSource audiosource;
    public AudioClip HunterThrow;
    public GameObject bullet;
    GameObject tempBullet; 
    public Transform firePoint;
    private float bulletSpeed = 6.5f;
    Vector3 lookPos;
    bool flag;
    float tiempo_start;
    float tiempo_end = 4;
    float coldownStart = 4;
    float coldownEnd = 4;
    GameObject player;
    public Vector3 playerPos;

    private void Start()
    {
        player = GameObject.Find("Player");
        flag = false;
        tiempo_end = 0.8f;
        coldownStart = 0.8f;
        coldownEnd = 0.8f;
    }

    void Update()
    {
        Vector3 playerPos = new Vector3(player.transform.position.x, player.transform.position.y);
        playerPos = playerPos - transform.position;
        float angle = Mathf.Atan2(playerPos.y, playerPos.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
        lookPos = Camera.main.ScreenToWorldPoint(mousePos) - transform.position;
        if (!flag)
        {
            tiempo_start = 0;
            if (coldownStart >= coldownEnd)
            {
                StartCoroutine(timeToShootAxe());
                flag = true;
            }
            else
            {
                coldownStart += Time.deltaTime;
            }
        }
        else
        {
            if (tiempo_start >= tiempo_end)
            {
                coldownStart = 0;
                flag = false;
            }
            else
            {
                tiempo_start += Time.deltaTime;
            }

        }


    }
    private void HunterShooting()
    {
        tempBullet = Instantiate(bullet, firePoint.position, firePoint.rotation);
        audiosource.PlayOneShot(AudioClip());
        Rigidbody2D rb = tempBullet.GetComponent<Rigidbody2D>();
        rb.AddForce(firePoint.right * bulletSpeed, ForceMode2D.Impulse);
    }

    AudioClip AudioClip()
    {
        return HunterThrow;
    }
    public IEnumerator timeToShootAxe()
    {
        yield return new WaitForSeconds(2f);
        HunterShooting();
    }
}