using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SantaClausBoss : Enemy
{
    public AudioSource audiosource;
    public AudioClip SantaDamage;
    public AudioClip SantaAppear;
    public Animator SantaClausAnim; 
    public static float tiempo;
    
    public static bool shootSnowBalls ;
    public static bool pushPresent;
    public GameObject Spawner;
    public static event Action BossSpawned = delegate { };


    void Start()
    {
        audiosource.PlayOneShot(AudioClip2());
        Spawner = GameObject.Find("SantaSpawner");
        //On Play Or Click Try Again
        shootSnowBalls = false;
        pushPresent = false; 

        health = 300;

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("bullet"))
        {
            SantaClausAnim.SetBool("Hit", true);
            TakeDamage(25);     //Temporal...
            audiosource.PlayOneShot(AudioClip());
            //Destroy(this.gameObject);
            Destroy(collision.gameObject); //Destruim la bala
        }

        //En impactar la bala amb el Enemic destrueix Enemy y la bala
        StartCoroutine(CoroutineAnimation());
    }

    private void Update()
    {
        
        Debug.Log(health);

        if (health <= 200 && health >= 100)
        {
            //TIRA BOLES DE NEU

            SantaClausAnim.SetBool("IsAtackingThrowSnow", true);
            shootSnowBalls = true; //Bool que li haurem de passar al Boss Shoot

        }
        if (health <= 100)
        {
            shootSnowBalls = false;
            SantaClausAnim.SetBool("IsAtackingThrowSnow", false);
            SantaClausAnim.SetBool("IsAtackingPushPresent", true);
            pushPresent = true; 
        }

    }

    public void OnDestroy()
    {
        BossSpawned.Invoke();
        Destroy(Spawner); 
    }

    AudioClip AudioClip()
    {
        return SantaDamage;
    }

    AudioClip AudioClip2()
    {
        return SantaAppear;
    }

    IEnumerator CoroutineAnimation()
    {
        yield return new WaitForSeconds(1);
        SantaClausAnim.SetBool("Hit", false);
    }
}





