using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PresentAttack : MonoBehaviour
{
    public GameObject presentBombaPref;
    public Transform spawnerPresentsPosition;
    public float spawn_time;
    public float tiempo;
    public Animator animRegalo;


    //TIME TO THROW : 
    bool flag;
    float tiempo_start;
    float tiempo_end = 4;
    float coldownStart = 4;
    float coldownEnd = 4;

    // Start is called before the first frame update
    void Start()
    {
        spawn_time = 2.0f; 
    }

    // Update is called once per frame
    void Update()
    {

        //tiempo = Time.time;
        //if (flag == true) tiempo = 0;

        //if (SantaClausBoss.pushPresent && tiempo > spawn_time)
        //{
        //    if (flag == false && tiempo < 50)
        //    {
        //        spawn_time = Time.time + 3.0f;

        //        PresentSpawn();
        //        

        //    }
        //}

        if (!flag)
        {
            tiempo_start = 0;
            if (coldownStart >= coldownEnd)
            {
                StartCoroutine(PushPresents());
                flag = true;
            }
            else
            {
                coldownStart += Time.deltaTime;
            }
        }
        else
        {
            if (tiempo_start >= tiempo_end)
            {
                coldownStart = 0;
                flag = false;
            }
            else
            {
                tiempo_start += Time.deltaTime;
            }

        }


    }

    

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == ("Ground"))
        {
            Destroy(this.gameObject);
            //Animation Explotar
            animRegalo.SetBool("Explota", true);   
        }

        if (collision.gameObject.tag == ("enemy"))
        {
            Destroy(this.gameObject);
        }
        if(collision.gameObject.tag ==("Player")) animRegalo.SetBool("Explota", true);

    }


    public void PresentSpawn()
    {
        Instantiate(presentBombaPref, new Vector3(spawnerPresentsPosition.transform.position.x, spawnerPresentsPosition.transform.position.y), Quaternion.identity);
    }

    public IEnumerator PushPresents()
    {
        yield return new WaitForSeconds(1f);
        PresentSpawn();
    }
}



