using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HunterSpawner : GameManager
{
    public GameObject cubo;
    public GameObject cam;
    public static float tiempo;
    float spawn_time;

    public bool santavivo = false;
    public bool huntervivo = false;

    void Start()
    {
        spawn_time = 25.0f;
        tiempo = 0;
    }

    public void OnEnable()
    {
        Hunter.eventHunterDie += CanSpawnHunter;
        SantaClausBoss.BossSpawned += SpawnHuntersAgain;
    }
    private void OnDisable()
    {
        Hunter.eventHunterDie -= CanSpawnHunter;
        SantaClausBoss.BossSpawned -= SpawnHuntersAgain;
    }
    public void CanSpawnHunter()
    {
        huntervivo = false;
    }
    public void SpawnHuntersAgain() { santavivo = false; }

    void Update()
    {
        tiempo = Time.time;
        Debug.Log("Hunter Time : " + tiempo);

        if (tiempo > spawn_time)
        {

            if (!huntervivo && santavivo == false)
            {

                spawn_time = Time.time + 5.0f;
                StartCoroutine(timeToSpawnHunters());
                huntervivo = true;
            }
            else if (tiempo + 100 > 100) santavivo = true; // IMPORTANT : PER QUE AIX� FUNCIONI CORRECTAMENT S'HA DE
                                                         //RESTABLIR EL TEMPS "tiempo" cada cop que fem click play again.
        }
    }

    public void HunterSpawn()
    {
        //StartCoroutine(waiter());
        Instantiate(cubo, new Vector3(cam.transform.position.x - 6f, cam.transform.position.y), Quaternion.identity);

    }

    IEnumerator timeToSpawnHunters()
    {
        yield return new WaitForSeconds(5f);
        HunterSpawn();
    }
}
