using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

public class Hunter : Character
{
    public AudioSource audiosource;
    public AudioClip HunterDamage;
    public Animator animator;
    public static event Action eventHunterDie = delegate { };
    void Start()
    {
        health = 100;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("bullet"))
        {
            animator.SetBool("Hit", true);
            TakeDamage(40);
            //audiosource.Play();
            audiosource.PlayOneShot(AudioClip());
            //Destroy(this.gameObject);
            Destroy(collision.gameObject); //Destruim la bala
        }

        //En impactar la bala amb el Enemic destrueix Enemy y la bala

        StartCoroutine(CoroutineAnimation());
    }

    private void OnDestroy()
    {
        eventHunterDie.Invoke();
    }

    AudioClip AudioClip()
    {
        return HunterDamage;
    }

    IEnumerator CoroutineAnimation()
    {
        yield return new WaitForSeconds(1);
        animator.SetBool("Hit", false);
    }
}




