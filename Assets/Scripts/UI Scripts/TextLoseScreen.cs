using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextLoseScreen : MonoBehaviour
{
    public Text text;
    public GameObject Guardado;

    void Awake()
    {
        text = GameObject.Find("Record").GetComponent<Text>();
        Guardado = GameObject.Find("Distancias");
    }

    void Start()
    {
        text.text = "Has recorregut "+Guardado.GetComponent<Text>().text;
    }
}
