using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DontDestroyThis : MonoBehaviour
{
    public GameObject text;
    public Text distance;
    private void Start()
    {
        text = GameObject.Find("Distancias");
        distance = GameObject.Find("DistanceText").GetComponent<Text>(); 
        DontDestroyOnLoad(text);

        text.gameObject.GetComponent<Text>().text = distance.text;
    }

    private void Update()
    {
        text.gameObject.GetComponent<Text>().text = distance.text;
    }
}
