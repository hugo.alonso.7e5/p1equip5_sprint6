using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    Player player;
    Text distanceText;
    GameObject backgroundMusic;
    bool pausa;
    public GameObject canvaMenu;
    public bool activado;
    public static int distance;

    private void Awake()
    {
        player = GameObject.Find("Player").GetComponent<Player>();
        distanceText = GameObject.Find("DistanceText").GetComponent<Text>();
        backgroundMusic = GameObject.Find("BackgroundMusic");
    }
    // Start is called before the first frame update
    void Start()
    {
        pausa = false;
        canvaMenu = GameObject.Find("pauseMenu");
        Time.timeScale = 1f;
    }

    // Update is called once per frame
    void Update()
    {

        canvaMenu.SetActive(pausa);
        if (pausa == false)
        {
            distance = Mathf.FloorToInt(player.distance)/4;

            distanceText.text = distance + " m";
            SetDistance(distance);
            //GameManager.gm.HunterFlag(distance);
            //GetDistance();

        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {

            Comprovar();

        }
    }
    public void Comprovar()
    {
        if (pausa == false)
        {
            PressPause();
        }
        else if (pausa == true)
        {
            PressStart();
        }
    }
    public void PressPause()
    {
        Time.timeScale = 0f;
        backgroundMusic.GetComponent<AudioSource>().Pause();
        pausa = true;
    }
    public void PressStart()
    {
        Time.timeScale = 1f;
        backgroundMusic.GetComponent<AudioSource>().UnPause();
        pausa = false;
    }

    public void SetDistance(int distanSe)
    {
        distance = distanSe; 
    }
    public int GetDistance()
    {
        return distance; 
    }

}
